#include "Blast.h"
//#include <unistd.h>//memory
#include <algorithm> //sort



const char Blast::nucleotids[DUMMY_NUCLEOTIDS] = { 'N','A', 'C', 'G', 'T'};
const int Blast::complements[DUMMY_NUCLEOTIDS] = { 0, 4, 3, 2, 1};
const int Blast::nucleotids_trans[256] = {      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0,
                                                    0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


Blast::Blast(Params* _params){
    this->isBigFile=false;
    this->params = _params;
    this->length_key = SIZE_KEY; //tamaño de la key - seed
	this->gapOpen = DEFAULT_MISMATCH; //DEFAULT_MISMATCH -4 //gap open o no coincidencia
	this->gapExtend = DEFAULT_MATCH; //DEFAULT_MATCH 1 //gap extend o coincidencia
	this->scoreThreshold = DEFAULT_MIN_SCORE; //DEFAULT_MIN_SCORE 50 //minimo valor de coincidencias a considerar en la key
	this->topScoresNum = DEFAULT_TOPSCORE_NUM; //DEFAULT_TOPSCORE_NUM 10 //Maximo numero de resultados a mostrar
	this->queryFile = 0;	/*query file name*/
	this->dbFile = 0;	/*database file name*/	
    this->db = 0;
	this->query = 0;  
    this->p=0;
	this->q=0;
    this->A = this->B = 0;
    this->H = 0;
    this->hashIdx.resize(0);    
    this->allResults.resize(0);	
}
Blast::~Blast(){
    delete[] db;
    delete[] query;
    if (p) {delete[] p;	}
    if (q) {delete[] q;	}
    if (A) {delete[] A;	}
    if (B) {delete[] B;	}
    if (H) {delete[] H;	}
    if (queryFile) {delete[] queryFile;};	
    if (dbFile) {delete[] dbFile;};
    db = NULL;
    query = NULL;
	p = NULL;
	q = NULL;
    A = NULL;
	B = NULL;
    H = NULL;
    queryFile = NULL;
    dbFile = NULL;
}

void Blast::load_parameters(){
    this->length_key = params->getLength_key();
	this->gapOpen = params->getGapOpen();
	this->gapExtend = params->getGapExtend();
	this->scoreThreshold = params->getScoreThreshold();
	this->topScoresNum = params->getTopScoresNum();
	this->queryFile = params->getQueryFile();	
	this->dbFile = params->getDbFile();
	
    std::cerr << "/************************************************/\n";
    if(params->getTBlast()==BLAST_CPU){
        std::cerr << "\t\tBLAST (CPU):\t\t\n";
    }else if(params->getTBlast()==BLAST_GPU){
        std::cerr << "\t\tBLAST (GPU):\t\t\n";
    }else if(params->getTBlast()==BLAST_OPENMP){
        std::cerr << "\t\tBLAST (OPENMP):\t\t\n";
    }else if(params->getTBlast()==BLAST_GPUOPENMP){
        std::cerr << "\t\tBLAST (GPU + OPENMP):\t\t\n";
    }
	std::cerr << "\tSize Key:\t\t\t "<< length_key<<"\t\n";
    std::cerr << "\tGap Open penalty(-):\t\t "<< gapOpen<<"\t\n";
	std::cerr << "\tGap Extension penalty(+):\t "<<gapExtend<<"\t\n";
    std::cerr << "\tScoreThreshold:\t\t\t "<<scoreThreshold<<"\t\n";
    std::cerr << "\ttopScoresNum:\t\t\t "<<topScoresNum<<"\t\n";
    std::cerr << "\tdbFile:\t\t\t\t"<<dbFile<<"\t\n";
    std::cerr << "\tqueryFile:\t\t\t "<<queryFile<<"\t\n";
	std::cerr << "/*************************************************/\n";
}

void Blast::load_files(){
    clock_t s;
    cout<<"\n\t Loading files ...\n";
	s = clock();
    this->db = new FastaFile(params->getDbFile());
    this->query = new FastaFile(params->getQueryFile());
    s = clock() - s;
    cout << "Length File db = "<< db->getLength_file() << "\t Length File query = "<<query->getLength_file()<< endl;
    cout << "Length pb seq db = "<< db->getLength_seq() << "\t Length pb seq query = "<< query->getLength_seq() << endl;
	cout << "time upload Files: "<< double(s)/double(CLOCKS_PER_SEC) << endl;  
    if(db->getLength_seq()>MAX_LENGTH_PROCESSED){
        this->isBigFile=true;
        this->lengthDbProcessed = uint32_t(MAX_LENGTH_PROCESSED);
        this ->nRepeatProccess = ( (int64_t (db->getLength_seq() ) )/ (int64_t(this->lengthDbProcessed) ) )+1;
        lengthLastProccess = (int64_t (db->getLength_seq() ) )% (int64_t(this->lengthDbProcessed) );
        cout<<"This File db is very big\n";
        cout<<"This file will be processed diferently\n";
        cout<<"repeat proccess = "<< this ->nRepeatProccess<<endl;
        cout<<"lengthlastprocess = "<< this ->lengthLastProccess<<endl;

        //exit(0);
    }else{
        this->lengthDbProcessed = db->getLength_seq();
        this ->nRepeatProccess = 1;
    }
}

void Blast::run(){
    clock_t s,t,u;
    load_parameters();
    load_files();
    load_seed();    
    if(load_search()){
        print_results();
    }        
    exit(0);
}

void Blast::fill_ADN (int *x, int n){
	srand(time(NULL)); //Inicia random	
	for(int i=0; i<n; ++i){
		*(x+i)=rand()%4;
	}
}   

string Blast::reverse( const string &s ){
  string rs = s; // allocate same amount of memory
  int rsCsr = 0;
  for ( int i = s.length() - 1; 
	i >= 0;
	i-- )
  {
    rs[ rsCsr ] = s[ i ];
    rsCsr++;
  }
  return( rs );
}

string Blast::revcomp( const string &s ){
	string rs = s;// allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
				rs[ rsCsr ] = 'T'; break;
			case 'T':
				rs[ rsCsr ] = 'A'; break;
			case 'C':
				rs[ rsCsr ] = 'G'; break;
			case 'G':
				rs[ rsCsr ] = 'C'; break;
			default:
				rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}

char Blast::revcomp( const char x ){
  switch( x )
  {
	  case 'A': return( 'T' ); 
	  case 'C': return( 'G' ); 
	  case 'G': return( 'C' ); 
	  case 'T': return( 'A' ); 
  }
	return( x );
}

bool Blast::isValid( const char x ){
  switch( x )
  {
     case 'A':
     case 'C':
     case 'G':
     case 'T':
       return( true );
  }
  return( false );
}

int Blast::getBaseIdx( char c ){
  switch( c ) {
  case 'N': return( 0 ); break;
  case 'A': return( 1 ); break;
  case 'C': return( 2 ); break;
  case 'G': return( 3 ); break;
  case 'T': return( 4 ); break;
  default: 
    cerr << "Unsupported character " << c << endl;
	  return( 0 );
  }
}

bool Blast::isKeyValid( const char *s, int len ){
  for( int i = 0; i < len; i++ )
  {
    if ( !isValid( s[ i ] ) ) { return( false ); }
  }
  return( true );
}

void Blast::print_results(){
    std::sort(allResults.begin(), allResults.end(), sortscoreThreshold);
    int num = allResults.size();
    uint32_t pa, pb;
    int score;
    char* x=0, *y=0;    
    cout<<"\n\tRESULTS\n"<<"Total ScoresNum detected = "<<num<<endl;
    for(int i = num<topScoresNum?num-1:topScoresNum-1 ; i>=0; --i){
        pa = std::get<0>(allResults[i]);
        pb = std::get<1>(allResults[i]);
        score = std::get<2>(allResults[i]);
        x = std::get<3>(allResults[i]);
        y = std::get<4>(allResults[i]);
        cout<<"\n\nscore = "<<score<<" , in segment of pb: "<<pa<<" to pb: "<<pb<<"\n";
        for(size_t j=0; j<score; ++j){
            cout<<x[j];
        }
        cout<<"\n";
        for(size_t j=0; j<score; ++j){
            cout<<y[j];
        }
        cout<<endl;
    }
    cout<<"\nEND\n";
}
