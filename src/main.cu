#include <iostream>
#include "GenericFunction.h"
#include "Params.h"
#include "Blast.h"
#include "BlastCpu.h"
#include "BlastGpu.h"
#include "BlastGpuOpenmp.h"
#include "BlastOpenmp.h"
#include <stdio.h>

int main(int argc, char* argv[]) {
	Params params;
	Blast* myblast = 0;	
	//CSearch* search = 0;

	//init graphics card device
	pInitDevice(argc, argv);
	GPUInfo* info = pGetGPUInfo();
	//parse parameters
	if (!params.parseParams(argc, argv)) {
		return -1;
	}
	int singleGPUID = params.getSingleGPUID();
	if(params.getTBlast()== BLAST_CPU ){
		myblast = new BlastCpu(&params);
	}else if(params.getTBlast()== BLAST_OPENMP){
		myblast = new BlastOpenmp(&params);
	}else if(params.getTBlast()== BLAST_GPU){
		if ((params.getNumGPUs() == 1)) {
			cout<<"Simple GPU detectado\n";
			if (singleGPUID >= info->n_device) {
				singleGPUID = info->n_device - 1;
			}
			fprintf(stderr,
					"Use a single compatible GPU with ID %d and %d CPU thread(s)\n",
					info->devices[singleGPUID], params.getNumCPUThreads());
			pSetDevice(info, info->devices[singleGPUID]); //select the specified compatible GPU

			/*run the search engine*/
			myblast = new BlastGpu(&params);
		} else if (params.getNumGPUs() > 1) {
			fprintf(stderr,
					"\nUse the first %d compatible GPUs and %d CPU thread(s)\n",
					info->n_device, params.getNumCPUThreads());
			/*run the search engine*/
			//myblast = new BlastMGPU(&params);
		} else {
			fprintf(stderr, "BLAST GPU No compatible device available.\n");
			return -1;
		}
	}else if(params.getTBlast()== BLAST_GPUOPENMP){
		if ((params.getNumGPUs() == 1)) {
			cout<<"Simple GPU detectado\n"<<"Number of Hosts CPUs: "<<params.getNumCPUThreads()<<" , total threads = "<<omp_get_num_procs()<<endl;
			if (singleGPUID >= info->n_device) {
				singleGPUID = info->n_device - 1;
			}
			fprintf(stderr,
					"Use a single compatible GPU with ID %d and %d CPU thread(s)\n",
					info->devices[singleGPUID], params.getNumCPUThreads());
			pSetDevice(info, info->devices[singleGPUID]); //select the specified compatible GPU

			/*run the search engine*/
			myblast = new BlastGpuOpenmp(&params);
		} else if (params.getNumGPUs() > 1) {
			fprintf(stderr,
					"\nUse the first %d compatible GPUs and %d CPU thread(s)\n",
					info->n_device, params.getNumCPUThreads());
			/*run the search engine*/
			//myblast = new BlastMGPU(&params);
		} else {
			fprintf(stderr, "BLAST GPU No compatible device available.\n");
			return -1;
		}
	}
	
	if (myblast) {
		myblast->run();
		delete myblast;
	}
	return 0;
}

/*
int main(int argc, char* argv[]) {
	Align* align;

#ifdef MPI_PARALLEL
	MPI_Init(&argc, &argv); //initialize the MPI runtime environment
#endif

	//create alignment object
	align = new Align();

	//parse the parameter
	if (align->parseParams(argc, argv) == false) {

		//release the object
		delete align;

#ifdef MPI_PARALLEL
		MPI_Finalize();
#endif
		return -1;
	}

	//run the kernel
	align->run();

	//release the object
	delete align;

#ifdef MPI_PARALLEL
	MPI_Finalize(); //finalize the MPI
#endif

	return 0;
}
*/

