#ifndef BLAST_GPU_OPENMP_H_
#define BLAST_GPU_OPENMPH_
#include "Params.h"
#include "fasta.h"
#include "Blast.h"
#include <cstdint>
#include <omp.h>
#include "GenericFunction.h"

using namespace std;

class BlastGpuOpenmp : public Blast{
protected:
private:
	uint32_t max_length_Hash();
	int similarityScore(const char *a, const char *b);
	int findMax(int *traceback, int length, int  &index);
	int load_seed();
	//virtual int load_seed(char* dbFile) {return 1;	}	
	int load_search();
	int Smith_Waterman_run(const char *A, const char *B, int n, char*& subseq_db,  char*& subseq_query);
	//virtual int load_search(char* query) {return 0;}

public:
	BlastGpuOpenmp(Params* params);
	virtual ~BlastGpuOpenmp();
	void run();
	static const char nucleotids[DUMMY_NUCLEOTIDS];
	static const int complements[DUMMY_NUCLEOTIDS];
	static const int nucleotids_trans[256];
	
};

#endif  // BLAST_GPU_OPENMP_H_
