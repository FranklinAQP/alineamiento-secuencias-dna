#include "BlastGpuOpenmp.h"
#include "Params.h"
#include "fasta.h"
#include <algorithm> //sort

#define CUERR do{ cudaError_t err;      \
  if ((err = cudaGetLastError()) != cudaSuccess) {    \
      int device; \  
      cudaGetDevice(&device); \
	  std::cout << "CUDA error on GPU " << device << ": "<< cudaGetErrorString(err) << ": " << __FILE__ << ", line: " << __LINE__ ; }}while(0);

__global__ void seededb(int *A, uint32_t *H){
	__shared__ char temp[BLOCK_SIZE + SIZE_KEY -1];
	int gindex = threadIdx.x + blockIdx.x *blockDim.x;
	int lindex = threadIdx.x;
	// Enviar elementos de entrada a la memoria compartida
	temp[lindex]  = A[gindex];
	if(threadIdx.x < SIZE_KEY-1) {
		temp[lindex + BLOCK_SIZE]  = A[gindex + BLOCK_SIZE];
	}
	// sincronizar  (Garantizar que todos los datos estén disponibles)
	__syncthreads();
	// Aplicar la plantilla
//	if(gindex < N - SIZE_KEY +1){
//	}
	int result  = 0;
    int fact = 1;
    for (int i = SIZE_KEY - 1 ; i >= 0  ; --i){
		result += fact * temp[lindex + i];
		fact *= 5;
	}
	H[gindex]  = result;
}

const char BlastGpuOpenmp::nucleotids[DUMMY_NUCLEOTIDS] = { 'N','A', 'C', 'G', 'T'};
const int BlastGpuOpenmp::complements[DUMMY_NUCLEOTIDS] = { 0, 4, 3, 2, 1};
const int BlastGpuOpenmp::nucleotids_trans[256] = {      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0,
                                                    0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

BlastGpuOpenmp::BlastGpuOpenmp(Params* _params) : Blast(_params){  }

BlastGpuOpenmp::~BlastGpuOpenmp(){ }

int BlastGpuOpenmp::load_seed(){
    clock_t t,u,v,w;;
    this->p = db->getSeq(); 
    uint64_t asize, hsize;
    if(isBigFile){
        asize = (uint64_t(MAX_LENGTH_PROCESSED + SIZE_KEY - 1) )*(uint64_t(sizeof(int) ) );    
        hsize = (uint64_t(MAX_LENGTH_PROCESSED + SIZE_KEY - 1) )*(uint64_t(sizeof(uint32_t) ) );
    }else{
        asize = (uint64_t(db->getLength_seq() ) )*(uint64_t(sizeof(int) ) );    
        hsize = (uint64_t(db->getLength_seq() ) )*(uint64_t(sizeof(uint32_t) ) );
    } 
    cout<< "Memory current proccesss de db (bytes) = "<<asize<<endl;  
    
    A  = (int *)malloc(asize);    
    H  = (uint32_t *)malloc(hsize);
    //uint32_t longHash = max_length_Hash();
    cout << "length Hash Table = "<<max_length_Hash()<<endl;
    //->vector <vector <uint32_t> > Hash(longHash);
    this->hashIdx.resize(max_length_Hash());
    uint32_t dbi = 0;
    uint32_t lengthDBcurrent = this->lengthDbProcessed;
    double time_preprocessing = 0,
           time_seed = 0,
           time_get_keyhash = 0,
           time_save_ptrhash = 0;
    int *d_A; 
    uint32_t *d_H;
    // Alloc space  for device  copies
	cudaMalloc ((void **)&d_A, asize);
    CUERR
    cudaMalloc ((void **)&d_H, hsize);
    CUERR	
    
    //cout<<"\nMemoria device on"<<endl;
    //std::cerr<<"Codificando pb N,A,C,G,T a int 0,1,2,3,4 \n";
    //std::cerr<<"trans A = "<<nucleotids_trans['A']<<" , trans(C) ="<<nucleotids_trans['C']<<" , trans(G) = "<<nucleotids_trans['G']<<" , trans(T) = "<<nucleotids_trans['T']<<" , trans(N) = "<<nucleotids_trans['N']<<endl;
    

    for(int r=this->nRepeatProccess; r>0; --r){    
        u = clock();
        if(this->isBigFile){
            if(r>1){
                lengthDBcurrent = this->lengthDbProcessed+SIZE_KEY-1;
            }else{
                lengthDBcurrent = this->lengthLastProccess;
            }
        }
        
        for (uint32_t i=0; i< lengthDBcurrent; ++i){
            *(A+i) = nucleotids_trans[*(p+dbi+i)];//DUMMY NUCLEOTID
        }        
        u = clock() - u;
        // Copy  to device
        cudaMemcpy (d_A, A, asize, cudaMemcpyHostToDevice);
        CUERR
        time_preprocessing += double(u)/double(CLOCKS_PER_SEC);
        //std::cout << "time preprocessed database: "<< double(u)/double(CLOCKS_PER_SEC) << endl;
        
        //cout << "Valores de 1000 first en secuencia A: "<<endl;
        //for(uint32_t i=0; i<db->getLength_seq(); ++i){
        //    cout << i << " -> " << *(A+i)<< "\t";
        // }
        //std::cerr<<"pb convertidos a int pb(0) = "<<*(A)<<" ; pb(2999999999) = "<< *(A+2999999999)<<"\n";
            
        //seed
        cout<<"Starting proccess GPU of seed "<<this->nRepeatProccess - r+1<<endl;	
        //determinando keyHash	
        t = clock();
///////////////////////////////OPENMP///////////////////////////////////////////////
        uint32_t ir = 0;
        


        #pragma omp parallel private(ir, v, w)
        {
            unsigned int cpu_thread_id = omp_get_thread_num();
            unsigned int num_cpu_threads = omp_get_num_threads();
            int section_length = lengthDBcurrent/num_cpu_threads;
 
            int *sub_A = A + cpu_thread_id * section_length;
            //unsigned int nbytes_per_kernel = nbytes / num_cpu_threads;
            unsigned int nbytes_per_kernel_A = asize / num_cpu_threads;
            unsigned int nbytes_per_kernel_H = hsize / num_cpu_threads;
            dim3 gpu_threads(16);  // 16 threads per block54
            dim3 gpu_blocks(lengthDBcurrent / (gpu_threads.x * num_cpu_threads));
            uint32_t initblockthread = section_length*cpu_thread_id;           
            
            //cudaMemset(d_A + initblockthread, 0, nbytes_per_kernel_H);
            #pragma omp critical
            {
                v = clock();
                //cout<<"thread "<<cpu_thread_id<<" inicio GPU con pos inicial = "<<initblockthread<<" y pos final = "<<initblockthread + section_length - 1<<endl;
                seededb<<<gpu_blocks, gpu_threads>>>(d_A + initblockthread, d_H + initblockthread);                        
                CUERR
            } 
                // Copy  result  back  to  host
                cudaMemcpy(H + initblockthread, d_H + initblockthread, nbytes_per_kernel_H, cudaMemcpyDeviceToHost);
                CUERR
                v = clock() - v;
                time_get_keyhash += double(v)/double(CLOCKS_PER_SEC);
                   
            //cout<<"thread "<<cpu_thread_id<<" Cambiando de zona critica"<<endl;
            //#pragma omp critical
            //{
                //cout<<"thread "<<cpu_thread_id<<" inicio Hashup"<<endl;
                //cout << "Valores de 100 first HASH: "<<endl;
                
                //agreagndoa Hash
                w = clock();
                for(uint32_t i=initblockthread; i < initblockthread + section_length;++i){//initblockthread + 50000;++i){//section_length;++i){
                    #pragma omp critical
                    hashIdx[*(H+i)].push_back(dbi+i);
                }
                w = clock() - w;
                time_save_ptrhash += double(w)/double(CLOCKS_PER_SEC);
                //cout<<"thread "<<cpu_thread_id<<" end Hashup"<<endl;
            //}        
        }
               
        
        t = clock() - t;
        //std::cout<<"Seeds agregadas a Hash table\n";
        time_seed += double(t)/double(CLOCKS_PER_SEC);
        //std::cout << "time generate seed: "<< double(t)/double(CLOCKS_PER_SEC) << endl;
        if(this->isBigFile){
            dbi += MAX_LENGTH_PROCESSED;
        }
    }
    std::cout << "\ntime preprocessed database: "<< time_preprocessing << endl;
    std::cout << "(seed 1) time get keyhash: "<< time_get_keyhash << endl;
    std::cout << "(seed 2) time save ptrhash: "<< time_save_ptrhash << endl;
    std::cout << "TOTAL time generate seed: "<< time_seed << endl;        
    free(A);free(H);
    cudaFree(d_A);cudaFree(d_H);
	CUERR
    
    //A=NULL;
    //H=NULL;
    return 1;
}

int BlastGpuOpenmp::load_search(){
    clock_t u;
    uint64_t bsize = uint64_t (query->getLength_seq()*sizeof(int) );
    cout<< "\nMemoria de query (bytes) = "<<bsize<<endl;
    B  = (int *)malloc(bsize);
    //Smith Waterman
    cout<<"Smith Waterman"<<endl;    
    u = clock();
    this->q = query->getSeq();
    //COnvertir char N,A,C,G,T a int 0,1,2,3,4 y grabar en b
    for (uint32_t i=0; i<query->getLength_seq(); ++i) {
        *(B+i) = nucleotids_trans[*(q+i)];
    }	

    //encontrar key de tabla hash a trabajar
    int keyb = 0, fact = 1;
    for (int i = SIZE_KEY ; i > 0  ; --i){
        keyb += fact * (*(B+i-1));
        fact *= 5;
    }
    free(B);
    //B = NULL;
    //int numCPUThreads = sysconf (_SC_NPROCESSORS_ONLN);
    //cout << "\nThreads: "<< numCPUThreads << endl;
    //COmparar query con cada fragmento de ADN de longitud de la query
    int nscore[hashIdx[keyb].size()];
    char* _subseq_db=new char [255];
    char* _subseq_query = new char [255];
    for(size_t i=0; i<hashIdx[keyb].size(); ++i){        
        nscore[i] = Smith_Waterman_run((p+hashIdx[keyb][i]), q, (int)(query->getLength_seq()), _subseq_db, _subseq_query);
        if(nscore[i]>=scoreThreshold){
            auto mytuple = std::make_tuple(hashIdx[keyb][i], hashIdx[keyb][i] + query->getLength_seq() -1, nscore[i], _subseq_db, _subseq_query);
            //std::tuple<uint32_t, uint32_t , int, string, string > 
            this->allResults.push_back(mytuple); 
        }
    }
    //delete[] _subseq_db;
    //delete[] _subseq_query;
    //_subseq_db = _subseq_query = NULL;
    u=clock()-u;
    cout << "time generate Smith Waterman: "<< double(u)/double(CLOCKS_PER_SEC) << endl; 
    return 1;
}

int BlastGpuOpenmp::Smith_Waterman_run(const char *A, const char *B, int n, char*& subseq_db,  char*& subseq_query){
    int matrix[n+1][n+1];
    int matrix_max, i_max, j_max;
    int type_index; ///tipo de rastreo con mayor indice
    ///inicializar a 0s la matriz
    for(int i=0;i<=n;i++){
        for(int j=0;j<=n;j++){
            matrix[i][j]=0;
        }
    }
    int traceback[4]; ///Para rastrear el mayor posible valor que tomaria el indice local
    int I_i[n+1][n+1];///Guarda i de secuencia de generación de mayor indice
    int I_j[n+1][n+1];///Guarda j de secuencia de generación de mayor indice

    ///Comparación de cadenas con key SUCCESS
    for (int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
            traceback[0] = matrix[i-1][j-1]+similarityScore((A+i-1),(B+j-1));
            traceback[1] = matrix[i-1][j]-gapOpen;
            traceback[2] = matrix[i][j-1]-gapOpen;
            traceback[3] = 0;
            matrix[i][j] = findMax(&(traceback[0]),4, type_index);
            switch(type_index){
                case 0:
                    I_i[i][j] = i-1;
                    I_j[i][j] = j-1;
                    break;
                case 1:
                    I_i[i][j] = i-1;
                    I_j[i][j] = j;
                    break;
                case 2:
                    I_i[i][j] = i;
                    I_j[i][j] = j-1;
                    break;
                case 3:
                    I_i[i][j] = i;
                    I_j[i][j] = j;
                    break;
            }
        }
    }
    /// imprime lo almacenado en consola
	/*
    for(int i=1;i<n;i++){
        for(int j=1;j<n;j++){
            printf("%d - ",matrix[i][j]);
        }
        printf("\n \n");
    }
	*/
    /// Encuentra el maximo score de la matriz
    matrix_max = 0;
    i_max=0; j_max=0;
    for(int i=1;i<=n;i++){//+1
        for(int j=1;j<=n;j++){//+1
            if(matrix[i][j]>matrix_max){
                matrix_max = matrix[i][j];
                i_max=i;
                j_max=j;
            }
        }
    }
	//cout<<"score max = "<<matrix_max<<endl;
    if(matrix_max>=scoreThreshold){
        int current_i=i_max,current_j=j_max;
        int next_i=I_i[current_i][current_j];
        int next_j=I_j[current_i][current_j];
        int tick=0;
        char consensus_a[n + n + 2],consensus_b[n + n + 2];

        while(((current_i!=next_i) || (current_j!=next_j)) && (next_j!=0) && (next_i!=0)){

            if(next_i==current_i)  consensus_a[tick] = '-';                  /// eliminacion in A
            else                   consensus_a[tick] = *(A + current_i - 1);   /// match/mismatch in A

            if(next_j==current_j)  consensus_b[tick] = '-';                  /// eliminacion in B
            else                   consensus_b[tick] = *(B + current_j-1);   /// match/mismatch in B

            current_i = next_i;
            current_j = next_j;
            next_i = I_i[current_i][current_j];
            next_j = I_j[current_i][current_j];
            tick++;
        }
        
        ///imprime ambas secuencias
        //cout<<"\nAlignment:"<<endl;
        //for(int i=0;i<n;i++){cout<<A[i];}; cout<<"  and"<<endl;//lengthSeqA
        //for(int i=0;i<n;i++){cout<<B[i];}; cout<<endl<<endl;//lengthSeqB
        strcpy(subseq_db,consensus_a);
        strcpy(subseq_query,consensus_b);
        /*
        cout<<"ticks = "<<tick<<endl;
        for(int i=tick-1;i>=0;i--) cout<<consensus_a[i];
        cout<<endl;
        for(int j=tick-1;j>=0;j--) cout<<consensus_b[j];
        cout<<endl;
        */
    }
	
	
    return matrix_max;
}

uint32_t BlastGpuOpenmp::max_length_Hash(){
	uint32_t result = 0, 
		     fact = 1;
	for (uint32_t i = 0 ; i < length_key  ; ++i){
		result += fact * 4;
		fact *= 5;
	}
	return result+1;
}

int BlastGpuOpenmp::similarityScore(const char *a, const char *b){
    int result;
    if((*a)==(*b)){
        result=gapExtend;
    }else{
        result=-gapOpen;
    }
    return result;
}

int BlastGpuOpenmp::findMax(int *traceback, int length, int  &index){
    int max = *traceback;
    index = 0;
    for(int i=1; i<length; ++i){
        if(*(traceback+i) > max){
            max = *(traceback+i);
            index=i;
        }
    }
    return max;
}
