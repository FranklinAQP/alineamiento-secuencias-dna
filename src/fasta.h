#ifndef FASTA_H_
#define FASTA_H_

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include <tuple>        // std::tuple, std::get, std::tie, std::ignore

#include "Params.h"

#define NEXTLINE 0
#define LINE 1
#define COMMENT 2
#define MAX_NAME_LENGTH				1023
#define MAX_SEQ_LENGTH				(256 * 1024)

using std::string; using std::cout;

class FastaFile{
private:
	char* namefile;
	const char* seq;
	bool isread_fastab;
	vector < tuple<size_t, string > >  comments;	
	uint32_t length_seq;
	uint32_t length_file;
	uint32_t lines;
	int read_fasta(char *filepath);
	string *read_fastab(char *filepath);
	//int pb_in_line;
public:
	static const char nucleotids[MAX_NUCLEOTIDS];
	static const int nucleotids_trans[256];
	
	FastaFile();
	FastaFile(char *filepath);	
	~FastaFile();	
	inline uint32_t getLength_file(){return length_file;}
	inline uint32_t getLength_seq(){return length_seq;}
	const char* getSeq(){return seq;}
	void loadFile(char *filepath);
	void write_fasta(string filepath, string *seq, string comment);
};

class CFastaFile{
private:
	static const char nucleotids[MAX_NUCLEOTIDS];
	static const int nucleotids_trans[256];
	unsigned char name[MAX_NAME_LENGTH + 1];
	unsigned char buf[MAX_SEQ_LENGTH + 1];
	int pos;
	FILE* file;
public:
	CFastaFile();
	~CFastaFile();

	int open(char *file);
	void close();
	unsigned char* getSeqName();
	unsigned char * nextSeq(int *length, int* alignedLength, int pad);
};

#endif  // FASTA_H_
