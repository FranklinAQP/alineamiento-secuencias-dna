#ifndef DEFS_H_
#define DEFS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#include <vector>
#include <omp.h>
#include <pthread.h>

using namespace std;

#define CUDABLAST_VERSION	"0.0.1"

//variables cudablast
#define BLOCK_SIZE 16 //bloque de threads
#define SIZE_KEY 5 //tama�o de la key - seed
#define DEFAULT_MIN_SCORE 10 //minimo valor de coincidencias a considerar para mostrar resultados
#define DEFAULT_TOPSCORE_NUM 10 //Maximo numero de resultados a mostrar
#define DEFAULT_MISMATCH 4 //gap open o no coincidencia
#define DEFAULT_MATCH 1 //gap extend o coincidencia
//end variables cudablast

//variables de input
#define MAX_LENGTH_PROCESSED	100000000 //maximo static path input -> 16gb ram
#define MAX_PATH_LENGTH	4095 //maximo path input
#define MIN_SEQ_QUERY_LENGTH 35 
#define	FILE_FORMAT_FASTA		1
#define FILE_FORMAT_FASTQ		2

//variables propios de DNA
#define MAX_NUCLEOTIDS     	4 //ACGT
#define DUMMY_NUCLEOTIDS   (MAX_NUCLEOTIDS + 1) //Desconocido N
//#define MATRIX_SIZE	(MAX_NUCLEOTIDS + 2)

/*threshold for GPU and CPU computing*/
#ifndef MAX_SEQ_LENGTH_THRESHOLD
#define MAX_SEQ_LENGTH_THRESHOLD    	3072
#endif

#endif /* DEFS_H_ */
