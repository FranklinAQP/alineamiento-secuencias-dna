#include "Params.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "GenericFunction.h"

Params::Params() {
	length_key = uint32_t( SIZE_KEY); //length of the key to seed
	gapOpen = DEFAULT_MISMATCH;//rest for unmatching
	gapExtend = DEFAULT_MATCH;//sum for matching
	scoreThreshold = DEFAULT_MIN_SCORE;//min score for result true
	topScoresNum = DEFAULT_TOPSCORE_NUM;//results views
	useSingleGPU = false; //default false;
	singleGPUID = 0; //default GPU ID;
	numGPUs = 1;
	numCPUThreads = sysconf (_SC_NPROCESSORS_ONLN);
	if(numCPUThreads < 1){
		std::cerr << "Failed to get the number of CPU cores and will use ONE\n";
		//fprintf(stderr, "Failed to get the number of CPU cores and will use ONE\n");
		numCPUThreads = 1;
	}else if(numCPUThreads > 4){
		//numCPUThreads = 4;//Modify max threads
		cout<<"numCPUThreads detected = "<<numCPUThreads<<endl;
	}
	useQueryProfile = true;
	tblast = BLAST_GPU;
	subMatName[0] = '\0';
	queryFile[0] = '\0';
	dbFile[0] = '\0';
}

Params::~Params() {
}

void Params::getSysTime(double *dtime) {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	*dtime = (double) tv.tv_sec;
	*dtime = *dtime + (double) (tv.tv_usec) / 1000000.0;
}

void Params::printUsage() {
	
	std::cerr << "./cudablast [options]\n";
	std::cerr << "Standard options:\n";
	std::cerr << "\t-help or -? \t: this information\n";
	std::cerr << "\t-test \t\t: run test of validation\n";
	std::cerr << "\t-tblast <string>\t\t: Type of blast implementation (cpu, gpu, openmp)\n";
	std::cerr << "\t-query <string>\t: sequence target file of consult\n";
	std::cerr << "\t-db <string>\t: database sequence file\n";
	std::cerr << "\t-qprf <int>\t: to use query profile instead of its variant, (default = "<< useQueryProfile <<")\n";
	std::cerr << "\t-gapo <int>\t: gap open penalty (0 ~ 255), (default = " << gapOpen << ")\n";
	std::cerr << "\t-gape <int>\t: gap extension penalty (0 ~ 255), (default = " << gapExtend << ")\n";
	std::cerr << "\t-min_score <int>\t: minimum score reported(default = " << scoreThreshold << ")\n";
	std::cerr << "\t-topscore_num <int>\t: number of top scores reported(default = " << topScoresNum << ")\n";
	std::cerr << "\t-use_single <int>\t: Use the compatible GPU with index #int, forcing num_gpus=1\n";
	std::cerr << "\t-num_gpus <int>\t: number of GPUs, (default = " << numGPUs << ")\n";
	std::cerr << "\t-num_threads <int>\t: number of CPU threads (default = " << numCPUThreads << ")\n";
	std::cerr << "\t-version\t: print out the version\n";
}

int Params::parseParams(int argc, char* argv[]) {
	bool queryAvail = false;
	bool dbAvail = false;
	int index;
	//display usages
	if (argc < 2 || !strcmp(argv[1], "-help") || !strcmp(argv[1], "-?")) {
		printUsage();
		return false;
	}
	GPUInfo* gpuInfo = pGetGPUInfo();

	index = 1;
	// parse other arguments
	if (!strcmp(argv[1], "-test") ) {
		printGPUInfo(gpuInfo);
		fprintf(stderr, "Running test of validation!");
		return false;
	}
	char* arg;
	for (; index < argc; index++) {
		arg = argv[index];
		if (index >= argc) {
			fprintf(stderr,
					"Number of arguments does not match!");
			printUsage();
			return 0;
		} else if (strcmp(arg, "-tblast") == 0) {
			char temptype[20];
			sscanf(argv[++index], "%s", temptype);
			string temptyp(temptype);
			if(temptyp.compare("cpu") == 0 || temptyp.compare("CPU") == 0){
				tblast = BLAST_CPU;
			}else if(temptyp.compare("openmp") == 0 || temptyp.compare("OPENMP") == 0){
				tblast = BLAST_OPENMP;
			}else if(temptyp.compare("gpuopenmp") == 0 || temptyp.compare("GPUOPENMP") == 0){
				tblast = BLAST_GPUOPENMP;
			}else if(temptyp.compare("gpu") == 0 || temptyp.compare("GPU") == 0){
				tblast = BLAST_GPU;//gpu
			}else{
				cout<<"error in tblast\n";
				exit(1);
			}
		}else if (strcmp(arg, "-query") == 0) {
			sscanf(argv[++index], "%s", queryFile);
			queryAvail = true;
		} else if (strcmp(arg, "-db") == 0) {
			sscanf(argv[++index], "%s", dbFile);
			dbAvail = true;
		} else if (strcmp(arg, "-gapo") == 0) {
			sscanf(argv[++index], "%d", &gapOpen);
			if (gapOpen < 0 || gapOpen > 255) {
				gapOpen = DEFAULT_MISMATCH;
				fprintf(stderr, "using the default gap open penalty: %d\n",
						gapOpen);
			}
		} else if (strcmp(arg, "-gape") == 0) {
			sscanf(argv[++index], "%d", &gapExtend);
			if (gapExtend < 0 || gapExtend > 255) {
				gapExtend = DEFAULT_MATCH;
				fprintf(stderr, "using the default gap extension penalty: %d\n",
						gapExtend);
			}
		} else if (strcmp(arg, "-min_score") == 0) {
			sscanf(argv[++index], "%d", &scoreThreshold);
			if (scoreThreshold < 0) {
				scoreThreshold = 0;
			}
		} else if (strcmp(arg, "-topscore_num") == 0) {
			sscanf(argv[++index], "%d", &topScoresNum);
			if (topScoresNum < 1) {
				topScoresNum = 1;
			}
		} else if (strcmp(arg, "-use_single") == 0) {
			sscanf(argv[++index], "%d", &singleGPUID);
			if (singleGPUID >= gpuInfo->n_device) {
				singleGPUID = gpuInfo->n_device - 1;
			}
			if (singleGPUID < 0) {
				singleGPUID = 0;
			}
			/*for the number of GPUs to be 1 if applicable*/
			numGPUs = min(1, gpuInfo->n_device);
			useSingleGPU = true;
		} else if (strcmp(arg, "-num_gpus") == 0) {
			sscanf(argv[++index], "%d", &numGPUs);
			if (numGPUs < 0) {
				numGPUs = 0;
			}
			if (numGPUs > gpuInfo->n_device) {
				numGPUs = gpuInfo->n_device;
			}
		} else if (strcmp(arg, "-num_threads") == 0) {
			sscanf(argv[++index], "%d", &numCPUThreads);
			if (numCPUThreads < 1) {
				numCPUThreads = 1;
			}
    } else if (strcmp(arg, "-qprf") == 0) {
			int value;
      		sscanf(argv[++index], "%d", &value);
			if(value != 0){
				useQueryProfile = true;
			}else{
				useQueryProfile = false;
			}
		} else if (strcmp(arg, "-help") == 0 || strcmp(arg, "-?") == 0) {
			printUsage();
			return 0;
		} else if (strcmp(arg, "-version") == 0) {
			fprintf(stderr, "CUDABLAST version: %s\n", CUDABLAST_VERSION);
			return 0;
		} else {
			fprintf(stderr, "\n************************************\n");
			fprintf(stderr, "Unknown option: %s;\n", arg);
			fprintf(stderr, "\n************************************\n");
			printUsage();
			return 0;
		}
	}

	if (queryAvail == false) {
		fprintf(stderr, "Please specify the query sequence file\n");
		printUsage();
		return 0;
	}
	if (dbAvail == false) {
		fprintf(stderr, "Please specifiy the database sequence file\n");
		printUsage();
		return 0;
	}
	/*print out compatible GPUs*/
	printGPUInfo(gpuInfo);

	/*globally adjust the number of GPUs to simplify the implemention*/
	if (numGPUs > 0 && gpuInfo->n_device > numGPUs) {
		gpuInfo->n_device = numGPUs;
	}

	/*check the number of threads*/
	if (numCPUThreads < numGPUs) {
		numCPUThreads = numGPUs;
	}
	if(numGPUs <= 1){
		useSingleGPU = true;
	}else{
		useSingleGPU = false;
	}


	return 1;
}

void Params::getMatrix(char* name, int matrix[32][32]) {
	int i, j;	
	for (i = 0; i < 32; i++) {
		for (j = 0; j < 32; j++) {
			matrix[i][j] = 0;
		}
	}
	fprintf(stderr, "*************************************************\n");
	fprintf(stderr, "the scoring matrix (%s) can not be found\n", name);
	fprintf(stderr, "*************************************************\n");
	//check the validaty of the matrix;
	/*
	for (i = 0; i < 32; i++) {
		for (j = 0; j <= i; j++) {
			if (matrix[i][j] != matrix[j][i]) {
				printf("values are not equal (%d %d)\n", matrix[i][j],
						matrix[j][i]);
				getchar();
				break;
			}
		}
	}*/
}
void Params::getMatrix(int matrix[32][32]) {
	getMatrix(getSubMatrixName(), matrix);
}
