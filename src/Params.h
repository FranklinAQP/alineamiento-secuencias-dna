#ifndef _PARAMS_H
#define _PARAMS_H
#include "Defs.h"
#include <iostream>


#define BLAST_GPU 0
#define BLAST_CPU 1
#define BLAST_OPENMP 2
#define BLAST_GPUOPENMP 3

class Params{
private:
	void printUsage();
	uint32_t length_key; // SIZE_KEY 4 //tamaño de la key - seed
	int gapOpen; //DEFAULT_MISMATCH -4 //gap open o no coincidencia
	int gapExtend; //DEFAULT_MATCH 1 //gap extend o coincidencia
	int scoreThreshold; //DEFAULT_MIN_SCORE 50 //minimo valor de coincidencias a considerar en la key
	int topScoresNum; //DEFAULT_TOPSCORE_NUM 10 //Maximo numero de resultados a mostrar
	bool useSingleGPU;
	int singleGPUID;
	int numCPUThreads;
	int numGPUs;
	bool useQueryProfile;

	int tblast;
	char subMatName[MAX_PATH_LENGTH];	/*scoring matrix name*/
	char queryFile[MAX_PATH_LENGTH];	/*query file name*/
	char dbFile[MAX_PATH_LENGTH];	/*database file name*/

public:
	Params();
	~Params();

	static void getSysTime(double * dtime);
	int parseParams(int argc, char* argv[]);
	int getLength_key(){return length_key;}
	int getGapOpen() {return gapOpen;}
	int getGapExtend() {return gapExtend;}
	int getTBlast() {return tblast;}

	char* getSubMatrixName() {return subMatName;}
	char* getQueryFile() {return queryFile;}
	char* getDbFile() {return dbFile;}

	void getMatrix(char* name, int matrix[32][32]);
	void getMatrix(int matrix[32][32]);
	int getScoreThreshold() {return scoreThreshold;}	
	int getTopScoresNum() {return topScoresNum;}

	void setScoreThreshold(int score) {scoreThreshold = score;}
	void setTopScoresNum(int num) {topScoresNum = num;}
	bool isUseSingleGPU() {return useSingleGPU;}
	int getSingleGPUID() {return singleGPUID;}
	int getNumCPUThreads() {return numCPUThreads;}
	int getNumGPUs() {return numGPUs;}
	bool isQueryProfile(){return useQueryProfile;}
};
#endif

