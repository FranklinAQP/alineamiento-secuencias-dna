#include "BlastCpu.h"
#include "Params.h"
#include "fasta.h"
#include <algorithm> //sort

const char BlastCpu::nucleotids[DUMMY_NUCLEOTIDS] = { 'N','A', 'C', 'G', 'T'};
const int BlastCpu::complements[DUMMY_NUCLEOTIDS] = { 0, 4, 3, 2, 1};
const int BlastCpu::nucleotids_trans[256] = {      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0,
                                                    0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

BlastCpu::BlastCpu(Params* _params) : Blast(_params){  }

BlastCpu::~BlastCpu(){ }

int BlastCpu::load_seed(){
    clock_t t,u,v,w;
    this->p = db->getSeq(); 
    uint64_t asize, hsize;
    if(isBigFile){
        asize = (uint64_t(MAX_LENGTH_PROCESSED + SIZE_KEY - 1) )*(uint64_t(sizeof(int) ) );    
        hsize = (uint64_t(MAX_LENGTH_PROCESSED + SIZE_KEY - 1) )*(uint64_t(sizeof(uint32_t) ) );
    }else{
        asize = (uint64_t(db->getLength_seq() ) )*(uint64_t(sizeof(int) ) );    
        hsize = (uint64_t(db->getLength_seq() ) )*(uint64_t(sizeof(uint32_t) ) );
    } 
    cout<< "Memory current proccesss de db (bytes) = "<<asize<<endl;  
    
    //cout<< "sizeof(char*) = "<<sizeof(char*)<<endl;  
    //cout<< "sizeof(int*) = "<<sizeof(int*)<<endl;
    //cout<< "sizeof(char) = "<<sizeof(char)<<endl;  
    //cout<< "sizeof(uint32_t) = "<<sizeof(uint32_t)<<endl;  
    A  = (int *)malloc(asize);    
    H  = (uint32_t *)malloc(hsize);
    //uint32_t longHash = max_length_Hash();
    cout << "length Hash Table = "<<max_length_Hash()<<endl;
    //->vector <vector <uint32_t> > Hash(longHash);
    this->hashIdx.resize(max_length_Hash());
    uint32_t dbi = 0;
    uint32_t lengthDBcurrent = this->lengthDbProcessed;
    double time_preprocessing = 0,
           time_seed = 0,
           time_get_keyhash = 0,
           time_save_ptrhash = 0;
    //std::cerr<<"Codificando pb N,A,C,G,T a int 0,1,2,3,4 \n";
    //std::cerr<<"trans A = "<<nucleotids_trans['A']<<" , trans(C) ="<<nucleotids_trans['C']<<" , trans(G) = "<<nucleotids_trans['G']<<" , trans(T) = "<<nucleotids_trans['T']<<" , trans(N) = "<<nucleotids_trans['N']<<endl;
    for(int r=this->nRepeatProccess; r>0; --r){    
        u = clock();
        if(this->isBigFile){
            if(r>1){
                lengthDBcurrent = this->lengthDbProcessed+SIZE_KEY-1;
            }else{
                lengthDBcurrent = this->lengthLastProccess;
            }
        }
        
        for (uint32_t i=0; i< lengthDBcurrent; ++i){
        //for (uint32_t i=0; i< 3000000000; ++i){
            *(A+i) = nucleotids_trans[*(p+dbi+i)];//DUMMY NUCLEOTID
        }
        
        u = clock() - u;
        time_preprocessing += double(u)/double(CLOCKS_PER_SEC);
        //std::cout << "time preprocessed database: "<< double(u)/double(CLOCKS_PER_SEC) << endl;
        
        //cout << "Valores de 1000 first en secuencia A: "<<endl;
        //for(uint32_t i=0; i<db->getLength_seq(); ++i){
        //    cout << i << " -> " << *(A+i)<< "\t";
        // }
        //std::cerr<<"pb convertidos a int pb(0) = "<<*(A)<<" ; pb(2999999999) = "<< *(A+2999999999)<<"\n";
            
        //seed
        cout<<"Starting proccess CPU of seed "<<this->nRepeatProccess - r+1<<endl;	
        //determinando keyHash	
        t = clock();
        v = clock();
        int result, fact;
        for( uint32_t i = 0; i < lengthDBcurrent - length_key + 1; ++i ){
            result  = 0;
            fact = 1;
            for (uint32_t j = i + length_key; j > i  ; --j){
                result += fact * (*(A+j-1));
                fact *= 5;
            }
            *(H+i)  = result;
        }
        v = clock()-v;
        //cout << "Valores de 100 first HASH: "<<endl;
        //for(uint32_t i=0; i<100; ++i){
        //    cout << i << " -> " << *(H+i)<< "\t";
        //}
        //agreagndoa Hash 
        w = clock();       
        for(uint32_t i=0;i<lengthDBcurrent - length_key + 1;++i){
            hashIdx[*(H+i)].push_back(dbi+i);
        }
        w = clock() - w;
        t = clock() - t;
        //std::cout<<"Seeds agregadas a Hash table\n";
        time_get_keyhash += double(v)/double(CLOCKS_PER_SEC);
        time_save_ptrhash += double(w)/double(CLOCKS_PER_SEC);
        time_seed += double(t)/double(CLOCKS_PER_SEC);
        //std::cout << "time generate seed: "<< double(t)/double(CLOCKS_PER_SEC) << endl;
        if(this->isBigFile){
            dbi += MAX_LENGTH_PROCESSED;
        }
    }    
    std::cout << "\ntime preprocessed database: "<< time_preprocessing << endl;
    std::cout << "(seed 1) time get keyhash: "<< time_get_keyhash << endl;
    std::cout << "(seed 2) time save ptrhash: "<< time_save_ptrhash << endl;
    std::cout << "TOTAL time generate seed: "<< time_seed << endl;
    
    free(A);free(H);
    
    //A=NULL;
    //H=NULL;
    return 1;
    //cout << "100 direcciones a Hash agregadas "<<endl;
    //for(int i=344; i<350; ++i){
	//		cout<<" key: "<<i<<" -> ";
	//		for (size_t it=0; it < Hash[i].size(); ++it){
	//			cout << ' ' << Hash[i][it] ;
	//		}				
	//		cout << '\n';
	//	}
}

int BlastCpu::load_search(){
    clock_t u;
    uint64_t bsize = uint64_t (query->getLength_seq()*sizeof(int) );
    cout<< "\nMemoria de query (bytes) = "<<bsize<<endl;
    B  = (int *)malloc(bsize);
    //Smith Waterman
    cout<<"Smith Waterman"<<endl;    
    u = clock();
    this->q = query->getSeq();
    
    //COnvertir char N,A,C,G,T a int 0,1,2,3,4 y grabar en b
    for (uint32_t i=0; i<query->getLength_seq(); ++i) {
        *(B+i) = nucleotids_trans[*(q+i)];
    }	
    //encontrar key de tabla hash a trabajar
    int keyb = 0, fact = 1;
    for (int i = SIZE_KEY ; i > 0  ; --i){
        keyb += fact * (*(B+i-1));
        fact *= 5;
    }
    free(B);
    
    
    //B = NULL;
    //int numCPUThreads = sysconf (_SC_NPROCESSORS_ONLN);
    //cout << "\nThreads: "<< numCPUThreads << endl;
    //COmparar query con cada fragmento de ADN de longitud de la query
    int nscore[hashIdx[keyb].size()];
    char* _subseq_db=new char [255];
    char* _subseq_query = new char [255];
    for(size_t i=0; i<hashIdx[keyb].size(); ++i){        
        nscore[i] = Smith_Waterman_run((p+hashIdx[keyb][i]), q, (int)(query->getLength_seq()), _subseq_db, _subseq_query);
        if(nscore[i]>=scoreThreshold){
            auto mytuple = std::make_tuple(hashIdx[keyb][i], hashIdx[keyb][i] + query->getLength_seq() -1, nscore[i], _subseq_db, _subseq_query);
            //std::tuple<uint32_t, uint32_t , int, string, string > 
            this->allResults.push_back(mytuple); 
        }
    }
    
    //delete[] _subseq_db;
    //delete[] _subseq_query;
    //_subseq_db = _subseq_query = NULL;
    u=clock()-u;
    cout << "time generate Smith Waterman: "<< double(u)/double(CLOCKS_PER_SEC) << endl; 
    return 1;
}

int BlastCpu::Smith_Waterman_run(const char *A, const char *B, int n, char*& subseq_db,  char*& subseq_query){
    int matrix[n+1][n+1];
    int matrix_max, i_max, j_max;
    int type_index; ///tipo de rastreo con mayor indice
    ///inicializar a 0s la matriz
    for(int i=0;i<=n;i++){
        for(int j=0;j<=n;j++){
            matrix[i][j]=0;
        }
    }
    int traceback[4]; ///Para rastrear el mayor posible valor que tomaria el indice local
    int I_i[n+1][n+1];///Guarda i de secuencia de generación de mayor indice
    int I_j[n+1][n+1];///Guarda j de secuencia de generación de mayor indice

    ///Comparación de cadenas con key SUCCESS
    for (int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
            traceback[0] = matrix[i-1][j-1]+similarityScore((A+i-1),(B+j-1));
            traceback[1] = matrix[i-1][j]-gapOpen;
            traceback[2] = matrix[i][j-1]-gapOpen;
            traceback[3] = 0;
            matrix[i][j] = findMax(&(traceback[0]),4, type_index);
            switch(type_index){
                case 0:
                    I_i[i][j] = i-1;
                    I_j[i][j] = j-1;
                    break;
                case 1:
                    I_i[i][j] = i-1;
                    I_j[i][j] = j;
                    break;
                case 2:
                    I_i[i][j] = i;
                    I_j[i][j] = j-1;
                    break;
                case 3:
                    I_i[i][j] = i;
                    I_j[i][j] = j;
                    break;
            }
        }
    }
    /// imprime lo almacenado en consola
	/*
    for(int i=1;i<n;i++){
        for(int j=1;j<n;j++){
            printf("%d - ",matrix[i][j]);
        }
        printf("\n \n");
    }
	*/
    /// Encuentra el maximo score de la matriz
    matrix_max = 0;
    i_max=0; j_max=0;
    for(int i=1;i<=n;i++){//+1
        for(int j=1;j<=n;j++){//+1
            if(matrix[i][j]>matrix_max){
                matrix_max = matrix[i][j];
                i_max=i;
                j_max=j;
            }
        }
    }
	//cout<<"score max = "<<matrix_max<<endl;
    if(matrix_max>=scoreThreshold){
        int current_i=i_max,current_j=j_max;
        int next_i=I_i[current_i][current_j];
        int next_j=I_j[current_i][current_j];
        int tick=0;
        char consensus_a[n + n + 2],consensus_b[n + n + 2];

        while(((current_i!=next_i) || (current_j!=next_j)) && (next_j!=0) && (next_i!=0)){

            if(next_i==current_i)  consensus_a[tick] = '-';                  /// eliminacion in A
            else                   consensus_a[tick] = *(A + current_i - 1);   /// match/mismatch in A

            if(next_j==current_j)  consensus_b[tick] = '-';                  /// eliminacion in B
            else                   consensus_b[tick] = *(B + current_j-1);   /// match/mismatch in B

            current_i = next_i;
            current_j = next_j;
            next_i = I_i[current_i][current_j];
            next_j = I_j[current_i][current_j];
            tick++;
        }
        
        ///imprime ambas secuencias
        //cout<<"\nAlignment:"<<endl;
        //for(int i=0;i<n;i++){cout<<A[i];}; cout<<"  and"<<endl;//lengthSeqA
        //for(int i=0;i<n;i++){cout<<B[i];}; cout<<endl<<endl;//lengthSeqB
        strcpy(subseq_db,consensus_a);
        strcpy(subseq_query,consensus_b);
        /*
        cout<<"ticks = "<<tick<<endl;
        for(int i=tick-1;i>=0;i--) cout<<consensus_a[i];
        cout<<endl;
        for(int j=tick-1;j>=0;j--) cout<<consensus_b[j];
        cout<<endl;
        */
    }
	
	
    return matrix_max;
}

uint32_t BlastCpu::max_length_Hash(){
	uint32_t result = 0, 
		     fact = 1;
	for (uint32_t i = 0 ; i < length_key  ; ++i){
		result += fact * 4;
		fact *= 5;
	}
	return result+1;
}

int BlastCpu::similarityScore(const char *a, const char *b){
    int result;
    if((*a)==(*b)){
        result=gapExtend;
    }else{
        result=-gapOpen;
    }
    return result;
}

int BlastCpu::findMax(int *traceback, int length, int  &index){
    int max = *traceback;
    index = 0;
    for(int i=1; i<length; ++i){
        if(*(traceback+i) > max){
            max = *(traceback+i);
            index=i;
        }
    }
    return max;
}
