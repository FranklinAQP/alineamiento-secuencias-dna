#ifndef BLAST_H_
#define BLAST_H_
#include "Params.h"
#include "fasta.h"
#include <cstdint>
#include "GenericFunction.h"

class Blast{
protected:
//private:
	bool isBigFile;
	Params* params;
	int nRepeatProccess;
	uint32_t lengthLastProccess;
	uint32_t lengthDbProcessed;//All or part of this DB
	uint32_t length_key; // SIZE_KEY 4 //tamaño de la key - seed
	int gapOpen; //DEFAULT_MISMATCH -4 //gap open o no coincidencia
	int gapExtend; //DEFAULT_MATCH 1 //gap extend o coincidencia
	int scoreThreshold; //DEFAULT_MIN_SCORE 50 //minimo valor de coincidencias a considerar en la key
	int topScoresNum; //DEFAULT_TOPSCORE_NUM 10 //Maximo numero de resultados a mostrar
	char *queryFile;	/*query file name*/
	char *dbFile;	/*database file name*/
	const char* p;
	const char* q;
	int *A, *B;
	uint32_t *H;
	std::vector <std::vector<uint32_t> > hashIdx;

	/*database information*/
	FastaFile* db;
	FastaFile* query;
	
	// vector of results <position init, position end, scoreThreshold, subseqdb, subseqquery>	
	std::vector <std::tuple<uint32_t, uint32_t, int, char*, char* > >  allResults;

	void load_parameters();
	void load_files();
	//int load_seed();
	virtual int load_seed() {return 1;	}	
	//int load_search();
	virtual int load_search() {return 0;}
	void print_results();
	/*virtual member functions*/	

	static bool sortscoreThreshold(const tuple<uint32_t, uint32_t, int, char*, char* > &a, const tuple<uint32_t, uint32_t, int, char*, char*> &b){
		return std::get<2>(a) < std::get<2>(b);
	}

	string *read_fasta(char *filepath, int& l );
	void fill_ADN (int *x, int n);
	string *blastsingle(string *input_seq, string *input_target);
	string reverse( const string &s );
	string revcomp( const string &s );
	char revcomp( const char x );
	bool isValid( const char x );
	int getBaseIdx( char c );
	bool isKeyValid( const char *s, int len );
	//uint32_t max_length_Hash();
	//int similarityScore(const char *a, const char *b);
	//int findMax(int *traceback, int length, int  &index);
	//int Smith_Waterman_run(const char *A, const char *B, int n, char*& subseq_db,  char*& subseq_query);

public:
	Blast(Params* params);
	virtual ~Blast();
	void run();
	static const char nucleotids[DUMMY_NUCLEOTIDS];
	static const int complements[DUMMY_NUCLEOTIDS];
	static const int nucleotids_trans[256];
	
};

#endif  // BLAST_H_