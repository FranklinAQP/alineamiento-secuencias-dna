#include "fasta.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using std::ios;
using std::ofstream;
using std::endl;

FastaFile::FastaFile(){
	this->lines = 0;
	//this->seq = 0;
	this->namefile = 0;
	this->length_seq = 0;
	this->length_file = 0;
	this->isread_fastab = false;
	//this->pb_in_line = 0;
}

FastaFile::FastaFile(char *filepath){
	isread_fastab=false;
	loadFile(filepath);
}

FastaFile::~FastaFile(){
	/*if(seq){
		char** seqs; = seq;
		free(seqs);
	}*/
	delete[] namefile;
	delete[] seq;
}

void FastaFile::loadFile(char *filepath){
	if(isread_fastab){
		string* temp = read_fastab(filepath);	
		this->length_seq = (uint32_t)temp->length();
		this->seq = temp->c_str();
	}else{
		//cout<<"\nleyendo archivo\n";
		read_fasta(filepath);
	}	
}

//In big files collapsed this method
string* FastaFile::read_fastab(char *filepath) {
    this->namefile = filepath;
	struct stat st;
    stat(filepath, &st);
	this->length_file = static_cast<uint32_t>(st.st_size);
	this->lines = 0;
    //int fd = open(filepath, O_RDONLY);  
	int fd = open(filepath, O_RDONLY);  
    //char *data = reinterpret_cast<char*>(mmap(NULL, length_file, PROT_WRITE, MAP_PRIVATE, fd, 0));
	char *data = reinterpret_cast<char*>(mmap(NULL, st.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0));
	//cout<<"data = "<<*data<<endl;
    char *lim = data + st.st_size;
    char *readptr = data - 1;
    char *writeptr = data;
    int state = NEXTLINE;
	char tempname[MAX_NAME_LENGTH];
	int posname = 0;
    while (readptr++ < lim) {
        switch (state) {
            case NEXTLINE:
                switch (*readptr) {
                    case '\r': case '\n':
						cout<<"salto";
                        state = NEXTLINE;
						this->lines++;
                        break;
                    case '>': case ';':
                        state = COMMENT;
                        break;
                    default:
                        *writeptr++ = *readptr;
                        state = LINE;
                }
                break;
            case LINE:
                switch (*readptr) {
                    case '\r': case '\n':
                        state = NEXTLINE;
						this->lines++;
                        break;
                    default:
                        *writeptr++ = *readptr;
                }
                break;
            case COMMENT:
                switch (*readptr) {
                    case '\r': case '\n':{
                        state = NEXTLINE;
						std::string strg(tempname);
						tuple<size_t,string> mytuple((size_t)this->lines, strg ); 
                		this->comments.push_back(mytuple);
						memset(tempname, '\0', sizeof(char)*MAX_NAME_LENGTH);
						posname=0;
						this->lines++;
						break;
					}
					default:
                        tempname[posname] = *readptr;
						++posname;
                }
                break;
        }
    }	
    return new string(data, writeptr - data - 1);
}

int FastaFile::read_fasta(char *filepath) {	
	this->namefile = filepath;	
	ifstream filex(filepath, ios::in );//| std::ifstream::binary   namefile_a
    filex.seekg(0,filex.end);///Calcula el tamano del archivo
    this->length_file = filex.tellg();
	this->lines = 0;
    filex.seekg(0,filex.beg);///Retorna el puntero al inicio del documento
	char *seqs;
	seqs = (char *)malloc(length_file * sizeof(char));
  
	char c; char tempname[MAX_NAME_LENGTH];
	int state = NEXTLINE;
	int posname = 0;
	uint32_t pos=0;
  	while (filex.get(c)){          // loop getting single characters
    	switch (state) {
            case NEXTLINE:
                switch (c) {
                    case '\r': case '\n':
                        state = NEXTLINE;
						this->lines++;
                        break;
                    case '>': case ';':
                        state = COMMENT;
                        break;
                    default:
                        *(seqs+pos) = c;
						++pos;
                        state = LINE;
                }
                break;
            case LINE:
                switch (c) {
                    case '\r': case '\n':
                        state = NEXTLINE;
						this->lines++;
                        break;
                    default:
                        *(seqs+pos) = c;
						++pos;
                }
                break;
            case COMMENT:
                switch (c) {
                    case '\r': case '\n':{
                        state = NEXTLINE;
						std::string strg(tempname);
						tuple<size_t,string> mytuple((size_t)this->lines, strg ); 
                		this->comments.push_back(mytuple);
						memset(tempname, '\0', sizeof(char)*MAX_NAME_LENGTH);
						posname=0;
						this->lines++;
						break;
					}
					default:
                        tempname[posname] << c;
						++posname;
                }
                break;
        }
	}
  	filex.close();
	seq=seqs;
	this->length_seq = pos;	
    return 1;
}

void FastaFile::write_fasta(string filepath, string *seq, string comment) {
    ofstream of;
    of.open(filepath.c_str(), ios::out);
    of << ">" << comment << endl;
    for (size_t i = 0; i < seq->length(); i += 80) {
        of << seq->substr(i, 80) << endl;
    }
    of.close();
}

const char CFastaFile::nucleotids[MAX_NUCLEOTIDS] = { 'A', 'C', 'G', 'T'};

const int CFastaFile::nucleotids_trans[256] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0,
												 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
												 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
CFastaFile::CFastaFile() {
	file = 0;
	pos = 0;
}
CFastaFile::~CFastaFile() {
	if (file) {
		fclose(file);
	}
}
int CFastaFile::open(char *fileName) {
	file = fopen(fileName, "rb");
	if (!file) {
		fprintf(stderr, "Opening file (%s) failed\n", fileName);
		goto out;
	}

	pos = 0;
	fseek(file, 0, SEEK_SET);
	return 1;
	out: return 0;
}
void CFastaFile::close() {
	if (file) {
		fclose(file);
	}
	file = 0;
}
unsigned char* CFastaFile::getSeqName() {
	return name;
}
unsigned char * CFastaFile::nextSeq(int *length, int* alignedLength, int pad) {
	int i, ch;
	unsigned char *p;

	//get the first meaningful line
	while (1) {
		if (feof(file)) {
			goto err;
		}
		if (!fgets((char*) buf, MAX_SEQ_LENGTH, file)) {
			goto err;
		}
		if (buf[0] != '\r' && buf[0] != '\n') {
			break;
		}
	}

	//check the sequence name mark '>'
	if (buf[0] != '>') {
		fprintf(stderr, "invalid fasta file format (line: %d)\n", __LINE__);
		goto err;
	}
	//read the sequence name
	p = buf;
	//remove the newline at the end of the buffer
	for (; *p && (*p != '\r' && *p != '\n'); p++)
		;
	*p = '\0';

	if (p == buf) {
		fprintf(stderr, "p == buf\n");
		goto err;
	}
	//copy the sequence name;
	strncpy((char*) name, (const char*) buf + 1, MAX_NAME_LENGTH);
	name[MAX_NAME_LENGTH] = '\0';

	//read the sequence
	pos = 0;
	while (1) {
		ch = fgetc(file);
		//check whether it is the end of file
		if (ch == EOF) {
			if (pos == 0) {
				fprintf(stderr, "invalid fasta file format (line: %d)\n",
						__LINE__);
				goto err;
			} else {
				//fprintf(stderr, "reaching the end of file\n");
				goto finish;
			}
		}
		//check whether it is the end of the sequence
		if (ch == '>') {
			if (pos == 0) {
				fprintf(stderr, "invalid fasta file format (line: %d)\n",
						__LINE__);
				goto err;
			} else {
				ungetc(ch, file);
				goto finish;
			}
		} else if (ch == '\n' || ch == '\r') {
			continue;
		}
		//
		ungetc(ch, file);
		//read a line of symbols
		if (!fgets(((char*) buf) + pos, MAX_SEQ_LENGTH, file)) {
			fprintf(stderr, "it is impossible\n");
			goto err;
		}
		//remove the newline
		p = buf + pos;
		for (; *p && (*p != '\r' && *p != '\n'); p++) {
			ch = nucleotids_trans[*p];
			if (ch == 0) {
				//fprintf(stderr, "invalid symbol: %c\n", *p);
				ch = DUMMY_NUCLEOTIDS;
			}
			*p = ch;
			pos++;
		}
	}

	finish: *length = pos;
	*alignedLength = pos;
	if (pad > 1) {
		int aligned = (pos + pad - 1) / pad;
		aligned *= pad;
		for (i = *length; i < aligned; i++) {
			buf[i] = DUMMY_NUCLEOTIDS;
		}
		*alignedLength = aligned;
	}
	return buf;
	err: *length = 0;
	*alignedLength = 0;
	return 0;
}

