##CUDABLAST

---

**This is a repository of parallell algorithm into the indexed BLAST algorithm with GPU CUDA.**

---

**AUTOR: Franklin Luis Antonio Cruz Gamero**

---

**Universidad Nacional de San Agustin**

---

## Requisitos

---

1. Sistema operativo **Ubuntu**.
2. **CUDA** version 8.0 o superior.

---

##INSTALACION

---

**Clonar en nuestra PC**

---

$ git clone https://FranklinAQP@bitbucket.org/FranklinAQP/alineamiento-secuencias-dna.git cudablast

---

**Ingresar al repositorio**

---

$ cd cudablast

---

**Compilar**

---

$ make

---

## Tutorial de uso para alineamiento de secuencias de ADN en archivos con formato fasta

---

**CPU -> $ ./cudablast -tblast cpu -db [db.fasta] -query [query.fasta]**

---

$ ./cudablast -tblast cpu -db sequence.fasta -query target.fasta

---

**GPU -> $ ./cudablast -tblast gpu -db [db.fasta] -query [query.fasta]**

---

$ ./cudablast -tblast gpu -db sequence.fasta -query target.fasta

---

**GPU -> $ ./cudablast -tblast openmp -db [db.fasta] -query [query.fasta]**

---

$ ./cudablast -tblast openmp -db sequence.fasta -query target.fasta

---


**Comandos mas usados desde consola para agregar, editar y clonar desde bitbucket**

ver video para [add delete edit online](https://youtu.be/0ocf7u76WSo)
con interfaz grafica [download and install first](https://www.sourcetreeapp.com/)

## Pasos para trabajar remotamente este repositorio
como [Clone a repository](https://confluence.atlassian.com/x/4whODQ).
[push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), 
o [add, commit,](https://confluence.atlassian.com/x/8QhODQ) 
y [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

**Clonar en nuestra PC**


$ git clone https://FranklinAQP@bitbucket.org/FranklinAQP/alineamiento-secuencias-dna.git

**Configurar usuario**


$ git config user.email 'redinfoaqp@gmail.com'
$ git config user.name 'Franklin'

**Para modificar archivos crear nueva rama**


$ git checkout -b 'nombrederama'      --> atajo de $ git branch iss53  ,   $ git checkout iss53

**Despues de realizar los cambios seleccionar lo archivos a guardar a hacer commit**


$ git add --all
$ git commit -a -m 'cambios fijados'

**Verificar si se tiene la ultima version del repositorio bitbucket**


$ git checkout master
$ git pull origin master

**en caso de error en el pull por indices distintos**


$ git add archivox && git add archivoy && git commit -m "resolved merge conflicts"

**Fusionar con  la rama prinsipal**


$ git merge nombrederama

**Actualizar el repositorio online de bitbucket**


$ git push -u origin master

---
